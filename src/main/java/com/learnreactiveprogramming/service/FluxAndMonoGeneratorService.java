package com.learnreactiveprogramming.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

public class FluxAndMonoGeneratorService {
    public Flux<String> nameFlux(){
        return Flux.fromIterable(List.of("alex","Deepak","Adam")).log();
    }

    public Flux<String> nameFluxUsingMap(){
        return Flux.fromIterable(List.of("alex", "Deepak", "Adam")).map(String::toUpperCase);
    }


    public Flux<String> nameFluxUsingMap_filter(int stringLength){
        return Flux.fromIterable(List.of("alex", "Deepak", "Adam")).map(String::toUpperCase).filter(name->name.length() > stringLength).log();
    }
    public Mono<String> nameMono_filter(int stringLength){
        return Mono.just("rabi")
                .map(String::toUpperCase)
                .filter(name->name.length() > stringLength)
                .log();
    }

    public Flux<String> nameMono_flatmapMany(int stringLength){
        return Mono.just("rabi")
                .map(String::toUpperCase)
                .filter(name->name.length() > stringLength)
                .flatMapMany(this::splitString)
                .log();
    }


    public Mono<List<String>> nameMono_filterWithFlatmap(int stringLength){
        return Mono.just("rabi")
                .map(String::toUpperCase)
                .filter(name->name.length() > stringLength)
                .flatMap(this::splitStringMono)
                .log();
    }
    public Mono<List<String>> splitStringMono(String name){
        var charArray = name.split("");
        var listOfcharArray = List.of(charArray);
        return Mono.just(listOfcharArray);
    }
    public Mono<List<String>> nameMono_filterWithFlatmapNew(int stringLength){
        return Mono.just("rabi")
                .map(String::toUpperCase)
                .filter(name->name.length() > stringLength)
                .map(this::splitStringMonoNew)
                .log();
    }


    public List<String> splitStringMonoNew(String name){
        var charArray = name.split("");
        var listOfcharArray = List.of(charArray);
        return listOfcharArray;
    }

    public Flux<String> nameFluxUsingFlatMap(int stringLength){
        return Flux.fromIterable(List.of("alex", "Deepak", "Adam","Omm", "alexa", "Marley", "ashley"))
                .map(String::toUpperCase)
                .filter(name->name.length() > stringLength)
                .flatMap(this::splitString)
                .log();
    }

    public Flux<String> nameFluxUsingFlatMap_async(int stringLength){
        return Flux.fromIterable(List.of("alex", "Deepak", "Adam","Omm", "alexa", "Marley", "ashley"))
                .map(String::toUpperCase)
                .filter(name->name.length() > stringLength)
                .flatMap(this::splitString_delay)
                .log();
    }

    public Flux<String> nameFluxUsingConcatmap(int stringLength){
        return Flux.fromIterable(List.of("alex", "Deepak", "Adam","Omm", "alexa", "Marley", "ashley"))
                .map(String::toUpperCase)
                .filter(name->name.length() > stringLength)
                .concatMap(this::splitString_delay)
                .log();
    }

    public Mono<String> nameMono(){
        return Mono.just("rabi").log();
    }

    public Flux<String> splitString(String name){
        var charArray = name.split("");
        return Flux.fromArray(charArray);
    }
    public Flux<String> splitString_delay(String name){
        var delay = new Random().nextInt(1000);
        var charArray = name.split("");
        return Flux.fromArray(charArray).delayElements(Duration.ofMillis(delay));
    }

    public Flux<String> nameFlux_transform(int stringLength){
        Function<Flux<String>,Flux<String>> filterMap = name -> name.map(String::toUpperCase).filter(s->s.length() > stringLength);

        return Flux.fromIterable(List.of("alex", "Deepak", "Adam"))
                .transform(filterMap)
                .flatMap(s->splitString(s))
                .defaultIfEmpty("default")
                .log();
    }

    public Flux<String> nameFlux_transform_switchIfEmpty(int stringLength){
        Function<Flux<String>,Flux<String>> filterMap = name -> name.map(String::toUpperCase)
                                                                    .filter(s->s.length() > stringLength)
                                                                    .flatMap(s->splitString(s));

        var defaultFlux = Flux.just("default").transform(filterMap);

        return Flux.fromIterable(List.of("alex", "Deepak", "Adam"))
                .transform(filterMap)
                .switchIfEmpty(defaultFlux)
                .log();
    }

    public Mono<String> nameMono_filter_defaultIfEmpty(int stringLength){
        Function<Mono<String>,Mono<String>> nameMonoFunction = name ->name.map(String::toUpperCase).filter(s->s.length() > stringLength);

        return Mono.just("rabi")
                .transform(nameMonoFunction)
                .defaultIfEmpty("default")
                .log();
    }

    public Mono<String> nameMono_filter_switchIfEmpty(int stringLength){
        Function<Mono<String>,Mono<String>> nameMonoFunction = name ->name.map(String::toUpperCase).filter(s->s.length() > stringLength);

        var defaultNameMano = Mono.just("default").transform(nameMonoFunction);

        return Mono.just("rabi")
                .transform(nameMonoFunction)
                .switchIfEmpty(defaultNameMano)
                .log();
    }


    public static void main(String[] args) {
        FluxAndMonoGeneratorService fluxAndMonoGeneratorService = new FluxAndMonoGeneratorService();
//        fluxAndMonoGeneratorService.nameFlux()
//                .subscribe(name -> System.out.println("Name is (by Flux): "+ name));
//        fluxAndMonoGeneratorService.nameFluxUsingMap()
//                .subscribe(name -> System.out.println("Name in Capital Case (by Flux): "+ name));
//        fluxAndMonoGeneratorService.nameMono()
//                .subscribe(name-> System.out.println("Name is (by Mono): " + name));

    }
}
