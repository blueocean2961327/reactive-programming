package com.learnreactiveprogramming.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

public class CombiningFluxAndMono {
    Flux<String> combiningTwoFluxConcat(){
        var elderFlux = Flux.just("Rabi","Narayan","Sadangi");
        var myFlux = Flux.just("Kumar", "Deepak");
        return Flux.concat(elderFlux,myFlux).log();
    }

    Flux<String> combiningTwoFluxConcatWith(){
        var elderFlux = Flux.just("Rabi","Narayan","Sadangi");
        var myFlux = Flux.just("Kumar", "Deepak");
        return elderFlux.concatWith(myFlux).log();
    }

    Flux<String> combiningTwoMonoConcatWith(){
        var elderMono = Flux.just("Rabi");
        var myMono = Mono.just("Deepak");
        return elderMono.concatWith(myMono).log();
    }

    Flux<String> combiningTwoFluxMerge(){
        var elderFlux = Flux.just("Rabi","Narayan","Sadangi").delayElements(Duration.ofMillis(100));
        var myFlux = Flux.just("Kumar", "Deepak").delayElements(Duration.ofMillis(150));
        return Flux.merge(elderFlux,myFlux).log();
    }

    Flux<String> combiningTwoFluxMergeWith(){
        var elderFlux = Flux.just("Rabi","Narayan","Sadangi").delayElements(Duration.ofMillis(100));
        var myFlux = Flux.just("Kumar","Deepak").delayElements(Duration.ofMillis(150));
        return elderFlux.mergeWith(myFlux).log();
    }

    Flux<String> combiningTwoMonoMergeWith(){
        var elderFlux = Mono.just("Rabi");
        var myFlux = Mono.just("Kumar");
        return Flux.merge(elderFlux,myFlux).log();
    }

}
