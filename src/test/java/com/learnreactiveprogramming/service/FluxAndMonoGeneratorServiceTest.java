package com.learnreactiveprogramming.service;

import org.junit.jupiter.api.Test;
import reactor.test.StepVerifier;

import java.util.List;

public class FluxAndMonoGeneratorServiceTest {
    FluxAndMonoGeneratorService fluxAndMonoGeneratorService = new FluxAndMonoGeneratorService();

    @Test
    void nameFluxTest() {
        var nameFlux = fluxAndMonoGeneratorService.nameFlux();
        StepVerifier.create(nameFlux).expectNext("alex","Deepak","Adam").verifyComplete();
    }

    @Test
    void nameFluxTestSingleData() {
        var nameFlux = fluxAndMonoGeneratorService.nameFlux();
        StepVerifier.create(nameFlux).expectNext("alex").verifyComplete();
    }

    @Test
    void nameMonoTest() {
        var nameMono = fluxAndMonoGeneratorService.nameMono();
        StepVerifier.create(nameMono).expectNext("rabi").verifyComplete();
    }

    @Test
    void nameFluxTestCount() {
        var nameFlux = fluxAndMonoGeneratorService.nameFlux();
        StepVerifier.create(nameFlux).expectNextCount(3).verifyComplete();
    }

    @Test
    void nameFluxTestCountMix() {
        var nameFlux = fluxAndMonoGeneratorService.nameFlux();
        StepVerifier.create(nameFlux)
                .expectNext("alex")
                .expectNextCount(2)
                .verifyComplete();
    }

    @Test
    void nameFluxTestNegative() {
        var nameFlux = fluxAndMonoGeneratorService.nameFlux();
        StepVerifier.create(nameFlux).expectNext("Deepak","Adam","alex").verifyComplete();
    }

    @Test
    void nameFluxUsingMapTest() {
        var nameFlux = fluxAndMonoGeneratorService.nameFluxUsingMap();
        StepVerifier.create(nameFlux).expectNext("ALEX","DEEPAK","ADAM").verifyComplete();
    }

    @Test
    void nameFluxUsingMap_filterTest(){
        int stringLength = 4;
        var nameFluxFilter = fluxAndMonoGeneratorService.nameFluxUsingMap_filter(stringLength);
        StepVerifier.create(nameFluxFilter).expectNext("DEEPAK").verifyComplete();
    }

    @Test
    void nameMonoUsingMap_filterTest(){
        int stringLength = 3;
        var nameFluxFilter = fluxAndMonoGeneratorService.nameMono_filter(stringLength);
        StepVerifier.create(nameFluxFilter).expectNext("RABI").verifyComplete();
    }

    @Test
    void nameFluxUsingFlatMap(){
        int stringLength = 4;
        var nameFluxFilter = fluxAndMonoGeneratorService.nameFluxUsingFlatMap_async(stringLength);
        StepVerifier.create(nameFluxFilter).expectNext("D","E","E","P","A","K","A","L","E","X","A","M","A","R","L","E","Y","A","S","H","L","E","Y")
                .verifyComplete();
    }

    @Test
    void nameFluxUsingFlatMap_async() {
        int stringLength = 4;
        var nameFluxFilter = fluxAndMonoGeneratorService.nameFluxUsingFlatMap_async(stringLength);
        StepVerifier.create(nameFluxFilter).expectNext("D", "E", "E", "P", "A", "K", "A", "L", "E", "X", "A", "M", "A", "R", "L", "E", "Y", "A", "S", "H", "L", "E", "Y")
                .verifyComplete();
    }

    @Test
    void nameFluxUsingFlatMap_async_Count(){
        int stringLength = 4;
        var nameFluxFilter = fluxAndMonoGeneratorService.nameFluxUsingFlatMap_async(stringLength);
        StepVerifier.create(nameFluxFilter).expectNextCount(23)
                .verifyComplete();
    }

    @Test
    void nameFluxUsing_asyncConcatMap(){
        int stringLength = 4;
        var nameFluxFilter = fluxAndMonoGeneratorService.nameFluxUsingConcatmap(stringLength);
        StepVerifier.create(nameFluxFilter).expectNextCount(23)
                .verifyComplete();
    }

    @Test
    void nameFluxUsingCountMapTest() {
        int stringLength = 4;
        var nameFluxFilter = fluxAndMonoGeneratorService.nameFluxUsingConcatmap(stringLength);
        StepVerifier.create(nameFluxFilter).expectNext("D", "E", "E", "P", "A", "K", "A", "L", "E", "X", "A", "M", "A", "R", "L", "E", "Y", "A", "S", "H", "L", "E", "Y")
                .verifyComplete();
    }

    @Test
    void nameMono_filterWithFlatmapTest(){
var stringLength = 3;
        var nameFluxFilter = fluxAndMonoGeneratorService.nameMono_filterWithFlatmap(stringLength);
        StepVerifier.create(nameFluxFilter).expectNext(List.of("R","A","B","I")).verifyComplete();
    }

    @Test
    void nameMono_flatmapMany() {
        var stringLength = 3;
        var nameMonoFlatmapMany = fluxAndMonoGeneratorService.nameMono_flatmapMany(stringLength);
        StepVerifier.create(nameMonoFlatmapMany).expectNext("R","A","B","I").verifyComplete();
    }

    @Test
    void nameFlux_transformTest() {
        int stringLength = 4;
        var nameFluxFilter = fluxAndMonoGeneratorService.nameFlux_transform(stringLength);
        StepVerifier.create(nameFluxFilter).expectNext("D", "E", "E", "P", "A", "K")
                .verifyComplete();
    }

    @Test
    void nameFlux_transformTest_1() {
        int stringLength = 6;
        var nameFluxFilter = fluxAndMonoGeneratorService.nameFlux_transform(stringLength);
        StepVerifier.create(nameFluxFilter)
//                .expectNext("D", "E", "E", "P", "A", "K")
                .expectNext("default")
                .verifyComplete();
    }

    @Test
    void nameFlux_transform_switchIfEmptyTest() {
        int stringLength = 6;
        var nameFluxFilter = fluxAndMonoGeneratorService.nameFlux_transform_switchIfEmpty(stringLength);
        StepVerifier.create(nameFluxFilter)
//                .expectNext("D", "E", "E", "P", "A", "K")
                .expectNext("D","E","F","A","U","L","T")
                .verifyComplete();
    }

    @Test
    void nameMono_filter_defaultIfEmptyTest() {
        int stringLength = 3;
        var nameFluxFilter = fluxAndMonoGeneratorService.nameMono_filter_defaultIfEmpty(stringLength);
        StepVerifier.create(nameFluxFilter)
                .expectNext("RABI")
//                .expectNext("D","E","F","A","U","L","T")
                .verifyComplete();
    }

    @Test
    void nameMono_filter_switchIfEmptyTest() {
        int stringLength = 4;
        var nameFluxFilter = fluxAndMonoGeneratorService.nameMono_filter_switchIfEmpty(stringLength);
        StepVerifier.create(nameFluxFilter)
//                .expectNext("RABI")
                .expectNext("DEFAULT")
                .verifyComplete();
    }


}
