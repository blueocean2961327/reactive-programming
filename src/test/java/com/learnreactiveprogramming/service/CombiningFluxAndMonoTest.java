package com.learnreactiveprogramming.service;

import org.junit.jupiter.api.Test;
import reactor.test.StepVerifier;

public class CombiningFluxAndMonoTest {
    CombiningFluxAndMono combiningFluxAndMono = new CombiningFluxAndMono();
   @Test
    void combiningTwoFluxConcatTest(){
       var concatFlux = combiningFluxAndMono.combiningTwoFluxConcat();

       StepVerifier.create(concatFlux).expectNext("Rabi","Narayan","Sadangi","Kumar", "Deepak").verifyComplete();
    }
    @Test
    void combiningTwoFluxConcatWithTest(){
        var concatFlux = combiningFluxAndMono.combiningTwoFluxConcatWith();

        StepVerifier.create(concatFlux).expectNext("Rabi","Narayan","Sadangi","Kumar", "Deepak").verifyComplete();
    }

    @Test
    void combiningTwoMonoConcatWithTest(){
        var concatFlux = combiningFluxAndMono.combiningTwoMonoConcatWith();

        StepVerifier.create(concatFlux).expectNext("Rabi","Deepak").verifyComplete();
    }

    @Test
    void combiningTwoFluxMergeTest(){
        var concatFlux = combiningFluxAndMono.combiningTwoFluxMerge();

        StepVerifier.create(concatFlux).expectNext("Rabi","Kumar","Narayan", "Deepak","Sadangi").verifyComplete();
    }

    @Test
    void combiningTwoFluxMergeWithTest(){
        var concatFlux = combiningFluxAndMono.combiningTwoFluxMergeWith();

        StepVerifier.create(concatFlux).expectNext("Rabi","Kumar","Narayan", "Deepak","Sadangi").verifyComplete();
    }
    
    @Test
    void combiningTwoMonoMergeWithTest(){
        var concatFlux = combiningFluxAndMono.combiningTwoMonoMergeWith();

        StepVerifier.create(concatFlux).expectNext("Rabi","Kumar").verifyComplete();
    }
}
